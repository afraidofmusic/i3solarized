# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
  # Grub2
  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "nodev";
    efiSupport = true;
  };
  boot.loader.efi = {
    canTouchEfiVariables = true;
    efiSysMountPoint = "/boot/efi";
  };
  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "/dev/disk/by-uuid/8f672680-dc75-4783-a151-f4db9566d75b";
      preLVM = true;
      allowDiscards = true;
    }
  ];
  networking = {
    hostName = "neptune"; # Define your hostname.
    supplicant.wlp2s0 = {
      configFile.path = "/etc/wpa_supplicant.conf";
      configFile.writable = true;
      userControlled.enable = true;
    };
  };

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "tr_q-latin5";
    defaultLocale = "en_GB.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Istanbul";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  git ntfs3g
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Xorg
  services.xserver = {
    enable = true;
    layout = "tr";
    libinput = {
      enable = true;
      naturalScrolling = true;
      tapping = true;
      accelProfile = "flat";
    };
    windowManager.i3.enable = true;
    desktopManager.xterm.enable = false;
  };

  # User
  users.users.can = {
    isNormalUser = true;
    description = "Can Aksu";
    home = "/home/can";
    extraGroups = [ "wheel" ];
    packages = [ pkgs.steam ];
  };
  # Dnscrypt-Proxy
  services.dnscrypt-proxy = {
    enable = true;
    resolverName = "dnscrypt.org-fr";
  };
  networking.nameservers = [ "127.0.0.1" ];
  # Virtualbox
  virtualisation.virtualbox.host.enable = true;
  # Logind
  services.logind.lidSwitch = "ignore";

  programs = {
    bash.enableCompletion = true;
    java.enable = true;
    vim.defaultEditor = true;
  };
  # Allow Non-Free
  nixpkgs.config.allowUnfree = true;
  # Intel Microcode
  hardware.cpu.intel.updateMicrocode = true;
  # Bumblebee
  hardware.bumblebee = {
    enable = true;
    pmMethod = "bbswitch";
  };
  # 32bit Game Support
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;
  nixpkgs.config.steam.primus = true;

  system.stateVersion = "18.03"; # Did you read the comment?
  system.autoUpgrade = {
    enable = true;
    channel = https://nixos.org/channels/nixos-18.03;
  };

}
