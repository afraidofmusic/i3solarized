{ pkgs, ... }:
  let
  CONFIG = "/home/can/.config/nixpkgs/config";
  in
{
  fonts.fontconfig.enableProfileFonts = true;
  home = {
    packages = with pkgs; [
    alsaUtils
    calcurse
    dunst
    feh
    flacon
    gtk-engine-murrine
    haskellPackages.ip
    htop
    iosevka
    libnotify
    mpc_cli
    mpv
    multimc
    ncmpcpp
    neofetch
    pamixer
    psmisc
    python3
    qbittorrent
    qutebrowser
    ranger
    redshift
    roboto
    rxvt_unicode
    streamlink
    terminus_font
    texlive.combined.scheme-full
    tmux
    unrar
    unzip
    weechat
    wpa_supplicant_gui
    xorg.xbacklight
    xorg.xsetroot
    zathura
    ];
    file = {
      ".Xresources".source = "${CONFIG}/xresources";
      ".bashrc".source = "${CONFIG}/bashrc";
      ".config/i3/config".source = "${CONFIG}/i3";
      ".config/i3status/config".source = "${CONFIG}/i3status";
      ".config/ncmpcpp/config".source = "${CONFIG}/ncmpcpp";
      ".config/qutebrowser/bookmarks/urls".source = "${CONFIG}/qutebrowser-bookmarks";
      ".config/qutebrowser/config.py".source = "${CONFIG}/qutebrowser.py";
      ".config/qutebrowser/style.css".source = "${CONFIG}/qutebrowser-style.css";
      ".config/zathura/zathurarc".source = "${CONFIG}/zathura";
      ".inputrc".source = "${CONFIG}/inputrc";
      ".tmux.conf".source = "${CONFIG}/tmux";
    };
  };
  gtk = {
    enable = true;
    font.name = "Terminus, 9";
    theme.name = "NumixSolarizedDark";
    theme.package = pkgs.numix-solarized-gtk-theme;
    gtk2 = {
      extraConfig = "gtk-toolbar-style = GTK_TOOLBAR_ICONS";
    };
  };
  qt = {
    enable = true;
    useGtkTheme = true;
  };
  programs = {
    home-manager = {
      enable = true;
      path = https://github.com/rycee/home-manager/archive/master.tar.gz;
    };
    htop = {
      enable = true;
      colorScheme = 6;
      showProgramPath = false;
      treeView = true;
    };
    vim = {
      enable = true;
      extraConfig = "
        \" Solarized
        syntax enable
        set background=dark
        colorscheme solarized
        set cursorline
        \" NerdTREE
        map <C-n> :NERDTreeToggle<CR>
        \" Airline Tabs
        let g:airline#extensions#tabline#enabled = 1
        \" Latex Preview Zathura
        let g:livepreview_previewer = 'zathura'
        \" Pane Switching
        nnoremap <C-J> <C-W><C-J>
        nnoremap <C-K> <C-W><C-K>
        nnoremap <C-L> <C-W><C-L>
        nnoremap <C-H> <C-W><C-H>
        set splitbelow
        set splitright
        \" Tweaks
        ";
      plugins = [
        "Solarized"
        "The_NERD_tree"
        "python-mode"
        "vim-airline"
        "vim-airline-themes"
        "vim-latex-live-preview"
      ];
    };
    git = {
      enable = true;
      userEmail = "canaksu@protonmail.com";
      userName = "Can AKSU";
    };
  };
  services = {
    dunst = {
      enable = true;
      settings = {
        global = {
	        font = "Terminus 9";
	        frame_width = 2;
	        geometry = "0x0-0+0";
	        horizontal_padding = 8;
	        icon_position = "off";
	        padding = 10;
          separator_color = "frame";
        };
        shortcuts = {
          close = "ctrl+space";
          close_all = "ctrl+shift+space";
        };
        urgency_normal = {
	        background  = "#002b36";
	        foreground  = "#839496";
	        frame_color = "#0e687f";
	        timeout = 10;
        };
        urgency_low = {
	        background  = "#002b36";
	        foreground  = "#839496";
	        frame_color = "#0e687f";
	        timeout = 10;
        };
        urgency_critical = {
	        background  = "#002b36";
	        foreground  = "#839496";
	        frame_color = "#dc322f";
	        timeout = 0;
        };
      };
    };
    redshift = {
      enable = true;
      latitude = "41";
      longitude = "28";
      temperature.night = 3000;
    };
    udiskie = {
      enable = true;
      tray = "never";
    };
    unclutter = {
      enable = true;
      timeout = 5;
    };
    mpd = {
      enable = true;
      musicDirectory = "/home/can/Music";
    };
  };
}
