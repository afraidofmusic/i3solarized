### Solarized
solarized = {
        'base03': '#002b36',
        'base02': '#073642',
        'base01': '#586e75',
        'base00': '#657b83',
        'base0': '#839496',
        'base1': '#93a1a1',
        'base2': '#eee8d5',
        'base3': '#fdf6e3',
        'yellow': '#b58900',
        'orange': '#cb4b16',
        'red': '#dc322f',
        'magenta': '#d33682',
        'violet': '#6c71c4',
        'blue': '#268bd2',
        'cyan': '#2aa198',
        'green': '#859900'
}
## Colors
c.colors.completion.category.bg = solarized['base03']
c.colors.completion.category.border.bottom = solarized['base02']
c.colors.completion.category.border.top = solarized['base03']
c.colors.completion.category.fg = solarized['base3']
c.colors.completion.even.bg = solarized['base03']
c.colors.completion.fg = solarized['base0']
c.colors.completion.item.selected.bg = solarized['blue']
c.colors.completion.item.selected.border.bottom = solarized['blue']
c.colors.completion.item.selected.border.top = solarized['blue']
c.colors.completion.item.selected.fg = solarized['base3']
c.colors.completion.match.fg = solarized['red']
c.colors.completion.odd.bg = solarized['base02']
c.colors.downloads.bar.bg = solarized['base03']
c.colors.downloads.error.bg = solarized['red']
c.colors.downloads.error.fg = solarized['base3']
c.colors.downloads.start.bg = solarized['blue']
c.colors.downloads.start.fg = solarized['base3']
c.colors.downloads.stop.bg = solarized['green']
c.colors.downloads.stop.fg = solarized['base3']
c.colors.hints.bg = solarized['base00']
c.colors.hints.fg = solarized['base03']
c.colors.hints.match.fg = solarized['base3']
c.colors.messages.error.bg = solarized['red']
c.colors.messages.error.border = solarized['red']
c.colors.messages.error.fg = solarized['base3']
c.colors.messages.info.bg = solarized['base03']
c.colors.messages.info.border = solarized['base02']
c.colors.messages.info.fg = solarized['base0']
c.colors.messages.warning.bg = solarized['yellow']
c.colors.messages.warning.border = solarized['yellow']
c.colors.messages.warning.fg = solarized['base3']
c.colors.prompts.bg = solarized['base03']
c.colors.prompts.fg = solarized['base0']
c.colors.prompts.selected.bg = solarized['base01']
c.colors.statusbar.command.bg = solarized['base03']
c.colors.statusbar.command.fg = solarized['base0']
c.colors.statusbar.command.private.bg = solarized['base03']
c.colors.statusbar.command.private.fg = solarized['base3']
c.colors.statusbar.insert.bg = solarized['base03']
c.colors.statusbar.insert.fg = solarized['base3']
c.colors.statusbar.normal.bg = solarized['base03']
c.colors.statusbar.normal.fg = solarized['base0']
c.colors.statusbar.private.bg = solarized['base03']
c.colors.statusbar.private.fg = solarized['base3']
c.colors.statusbar.progress.bg = solarized['base0']
c.colors.statusbar.url.error.fg = solarized['red']
c.colors.statusbar.url.fg = solarized['base0']
c.colors.statusbar.url.hover.fg = solarized['blue']
c.colors.statusbar.url.success.http.fg = solarized['cyan']
c.colors.statusbar.url.success.https.fg = solarized['green']
c.colors.statusbar.url.warn.fg = solarized['orange']
c.colors.tabs.bar.bg = solarized['base1']
c.colors.tabs.even.bg = solarized['base01']
c.colors.tabs.even.fg = solarized['base2']
c.colors.tabs.indicator.error = solarized['red']
c.colors.tabs.indicator.start = solarized['blue']
c.colors.tabs.indicator.stop = solarized['green']
c.colors.tabs.odd.bg = solarized['base00']
c.colors.tabs.odd.fg = solarized['base2']
c.colors.tabs.selected.even.bg = solarized['base03']
c.colors.tabs.selected.even.fg = solarized['base2']
c.colors.tabs.selected.odd.bg = solarized['base03']
c.colors.tabs.selected.odd.fg = solarized['base2']
### Fonts
c.fonts.completion.category = 'Bold 9pt Terminus'
c.fonts.completion.entry = '9pt Terminus'
c.fonts.debug_console = '9pt Terminus'
c.fonts.downloads = '9pt Terminus'
c.fonts.hints = 'Bold 10pt Terminus'
c.fonts.keyhint = '9pt Terminus'
c.fonts.messages.error = '9pt Terminus'
c.fonts.messages.info = '9pt Terminus'
c.fonts.messages.warning = '9pt Terminus'
c.fonts.prompts = '9pt Terminus'
c.fonts.statusbar = '9pt Terminus'
c.fonts.tabs = '9pt Terminus'
c.fonts.web.size.default = 16
c.fonts.web.size.default_fixed = 16
### Hints
c.hints.border = '2px solid #00E3BE23'
c.hints.chars = 'asdfghjklqwertyuopzxcvbnm'
c.hints.mode = 'letter'
c.hints.scatter = True
### Prompt
c.prompt.filebrowser = True
c.prompt.radius = 0
### Statusbar
c.statusbar.padding = {'bottom': 2, 'left': 2, 'right': 2, 'top': 2}
c.statusbar.widgets = ["progress", "keypress", "url"]
### Tabs
c.tabs.background = True
c.tabs.indicator.width = 0
c.tabs.padding = {'bottom': 2, 'left': 3, 'right': 2, 'top': 2}
c.tabs.title.alignment = 'left'
c.tabs.wrap = False
c.tabs.favicons.show = 'never'
### Downloads
c.downloads.position = 'bottom'
### Completion
c.completion.cmd_history_max_items = 0
c.completion.height = '33%'
c.completion.scrollbar.width = 0
c.completion.shrink = True
c.completion.web_history_max_items = 0
### Window
c.window.title_format = '{private}{perc}{title}{title_sep}qutebrowser'
### Web
c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko'
### Search Engines
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}',
                       '4c': 'https://4chan.org/{}/catalog',
                       'aw': 'https://wiki.archlinux.org/?search={}',
                       'gen': 'https://genius.com/search?q={}',
                       'gw': 'https://wiki.gentoo.org/?search={}', 'w':
                       'https://en.wikipedia.org/?search={}',
                       'r': 'https://reddit.com/r/{}',
                       'scp': 'http://scp-wiki.net/scp-{}',
                       'tpb': 'https://thepiratebay.org/search/{}',
                       'w': 'https://en.wikipedia.org/?search={}',
                       'wtr': 'https://tr.wikipedia.org/?search={}',
                       'yt': 'https://youtube.com/results?search_query={}'}
### Misc
c.confirm_quit = ['multiple-tabs', 'downloads']
c.content.user_stylesheets = ['~/.config/qutebrowser/style.css']
c.editor.command = ['urxvt', '-e', 'vim', '{file}']
### Keybinds
## Unbind Keys
config.unbind('d', mode='normal')
config.unbind('gl', mode='normal')
config.unbind('gr', mode='normal')
## Bind Keys
config.bind('dd', 'tab-close')
config.bind('gj', 'tab-move +')
config.bind('gk', 'tab-move -')
config.bind('zz', 'config-cycle statusbar.hide true false')
config.bind('ZZ', 'config-cycle tabs.show always never')
## Streamlink
config.bind(',A', 'spawn streamlink {url} 360p --player="mpv --cache 2048"')
config.bind(',B', 'spawn streamlink {url} 480p --player="mpv --cache 2048"')
config.bind(',C', 'spawn streamlink {url} 720p --player="mpv --cache 2048"')
config.bind(',D', 'spawn streamlink {url} 1080p --player="mpv --cache 2048"')
config.bind(',a', 'hint links spawn streamlink {hint-url} 360p')
config.bind(',b', 'hint links spawn streamlink {hint-url} 480p')
config.bind(',c', 'hint links spawn streamlink {hint-url} 720p')
config.bind(',d', 'hint links spawn streamlink {hint-url} 1080p')

